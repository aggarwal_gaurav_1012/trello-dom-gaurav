const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

document.addEventListener("DOMContentLoaded", function () {
    const urlParams = new URLSearchParams(window.location.search);
    const boardId = urlParams.get("boardId");

    fetchBoardsSidebar();
    fetchLists(boardId);
    fetchBoardTitle(boardId);

    document.getElementById("create-list-form").addEventListener("submit", function (event) {
        event.preventDefault();
        const listName = document.getElementById("list-name").value;
        createList(boardId, listName);
    });
});

function fetchBoardsSidebar() {
    const url = `https://api.trello.com/1/members/me/boards?fields=name,url&key=${apiKey}&token=${apiToken}`;

    fetch(url)
        .then(response => response.json())
        .then(boards => {
            populateSidebar(boards);
        })
        .catch(error => console.error('Error fetching boards:', error));
}

function populateSidebar(boards) {
    const boardsList = document.getElementById("boards-list");
    boardsList.innerHTML = '';

    boards.forEach(board => {
        const boardListItem = document.createElement('li');
        boardListItem.className = 'hover:text-gray-400 cursor-pointer flex justify-between items-center';
        boardListItem.dataset.id = board.id;
        boardListItem.innerHTML = `<a href="boards_page.html?boardId=${board.id}" class="flex-1">${board.name}</a>`;

        const deleteButton = document.createElement('img');
        deleteButton.src = './images/trash_icon.png';
        deleteButton.alt = 'Delete';
        deleteButton.className = 'cursor-pointer w-4 h-4';

        deleteButton.addEventListener('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            deleteBoard(board.id);
        });

        boardListItem.appendChild(deleteButton);
        boardsList.appendChild(boardListItem);
    });
}

function fetchLists(boardId) {
    const url = `https://api.trello.com/1/boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`;

    fetch(url)
        .then(response => response.json())
        .then(lists => {
            displayLists(lists);
        })
        .catch(error => console.error('Error fetching lists:', error));
}

function fetchBoardTitle(boardId) {
    const url = `https://api.trello.com/1/boards/${boardId}?fields=name&key=${apiKey}&token=${apiToken}`;

    fetch(url)
        .then(response => response.json())
        .then(board => {
            document.getElementById("board-title").textContent = board.name;
        })
        .catch(error => console.error('Error fetching board title:', error));
}

function displayLists(lists) {
    const listsContainer = document.querySelector('.flex.overflow-x-auto');
    listsContainer.innerHTML = '';

    lists.forEach(list => {
        const listElement = createListElement(list);
        listsContainer.appendChild(listElement);
    });

    const addListButton = document.createElement('button');
    addListButton.className = 'text-blue-400 cursor-pointer h-12 p-2';
    addListButton.textContent = '+ Add another list';

    addListButton.addEventListener('click', () => {
        document.getElementById("create-list-form-container").classList.toggle("hidden");
    });
    listsContainer.appendChild(addListButton);
}

function createList(boardId, listName) {
    const url = `https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`;

    fetch(url, {
        method: 'POST'
    })
        .then(response => response.json())
        .then(list => {
            console.log('List created:', list);
            addListToDOM(list);
            document.getElementById("create-list-form").reset();
            document.getElementById("create-list-form-container").classList.add("hidden");
        })
        .catch(error => console.error('Error:', error));
}

function addListToDOM(list) {
    const listElement = createListElement(list);
    const listsContainer = document.querySelector('.flex.overflow-x-auto');
    listsContainer.insertBefore(listElement, listsContainer.lastElementChild);
}

function createListElement(list) {
    const listContainer = document.createElement('div');
    listContainer.className = 'bg-gray-700 w-60 p-2 rounded-md text-white list flex flex-col';
    listContainer.dataset.id = list.id;

    const listHeader = document.createElement('div');
    listHeader.className = 'flex justify-between items-center mb-5';

    const listTitle = document.createElement('div');
    listTitle.className = 'text-lg flex-1';
    listTitle.textContent = list.name;

    const deleteListButton = document.createElement('img');
    deleteListButton.src = './images/trash_icon.png';
    deleteListButton.alt = 'Delete';
    deleteListButton.className = 'cursor-pointer w-4 h-4';

    deleteListButton.addEventListener('click', () => {
        deleteList(list.id);
    });

    listHeader.appendChild(listTitle);
    listHeader.appendChild(deleteListButton);
    listContainer.appendChild(listHeader);

    fetchCards(list.id, listContainer);

    const addCardButton = document.createElement('button');
    addCardButton.className = 'text-blue-400 cursor-pointer h-12 p-2';
    addCardButton.textContent = '+ Add Card';

    addCardButton.addEventListener('click', () => {
        const cardFormContainer = listContainer.querySelector('.create-card-form-container');
        cardFormContainer.classList.toggle('hidden');
    });

    const cardFormContainer = document.createElement('div');
    cardFormContainer.className = 'create-card-form-container hidden mt-4';
    cardFormContainer.innerHTML = `
        <form class="create-card-form flex space-x-2">
            <input type="text" class="card-name p-2 rounded bg-gray-800 text-white w-36" placeholder="Card name" required>
            <button type="submit" class="bg-blue-500 text-white h-10 p-2 rounded">Create</button>
        </form>
    `;

    cardFormContainer.querySelector('.create-card-form').addEventListener('submit', (event) => {
        event.preventDefault();
        const cardName = cardFormContainer.querySelector('.card-name').value;
        createCard(list.id, cardName, cardFormContainer);
    });

    listContainer.appendChild(cardFormContainer);
    listContainer.appendChild(addCardButton);

    return listContainer;
}

function fetchCards(listId, listContainer) {
    const url = `https://api.trello.com/1/lists/${listId}/cards?key=${apiKey}&token=${apiToken}`;

    fetch(url)
        .then(response => response.json())
        .then(cards => {
            displayCards(cards, listContainer);
        })
        .catch(error => console.error('Error fetching cards:', error));
}

function displayCards(cards, listContainer) {
    cards.forEach(card => {
        const cardElement = createCardElement(card);
        listContainer.appendChild(cardElement);
    });
}

function createCard(listId, cardName, cardFormContainer) {
    const url = `https://api.trello.com/1/cards?idList=${listId}&name=${cardName}&key=${apiKey}&token=${apiToken}`;

    fetch(url, {
        method: 'POST'
    })
        .then(response => response.json())
        .then(card => {
            console.log('Card created:', card);
            addCardToDOM(card, cardFormContainer);
            cardFormContainer.querySelector('.create-card-form').reset();
            cardFormContainer.classList.add('hidden');
        })
        .catch(error => console.error('Error:', error));
}

function addCardToDOM(card, cardFormContainer) {
    const cardElement = createCardElement(card);
    cardFormContainer.parentElement.insertBefore(cardElement, cardFormContainer);
}

function createCardElement(card) {
    const cardElement = document.createElement('div');
    cardElement.className = 'bg-gray-800 p-2 rounded mb-2 text-white flex justify-between items-center card';
    cardElement.dataset.id = card.id;
    cardElement.textContent = card.name;

    const deleteCardButton = document.createElement('img');
    deleteCardButton.src = './images/trash_icon.png';
    deleteCardButton.alt = 'Delete';
    deleteCardButton.className = 'cursor-pointer w-4 h-4';

    deleteCardButton.addEventListener('click', () => {
        deleteCard(card.id);
    });

    cardElement.appendChild(deleteCardButton);

    return cardElement;
}

function deleteCard(cardId) {
    const url = `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${apiToken}`;

    fetch(url, {
        method: 'DELETE'
    })
        .then(() => {
            console.log('Card deleted');
            const cardElement = document.querySelector(`.card[data-id="${cardId}"]`);
            if (cardElement) {
                cardElement.remove();
            } else {
                console.error('Error: Card element not found');
            }
        })
        .catch(error => console.error('Error deleting card:', error));
}

function deleteBoard(boardId) {
    const url = `https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${apiToken}`;

    fetch(url, {
        method: 'DELETE'
    })
        .then(() => {
            console.log('Board deleted');
            location.reload();
        })
        .catch(error => console.error('Error deleting board:', error));
}

function deleteList(listId) {
    const url = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=${apiKey}&token=${apiToken}`;

    fetch(url, {
        method: 'PUT'
    })
        .then(() => {
            console.log('List deleted');
            document.querySelector(`.list[data-id="${listId}"]`).remove();
        })
        .catch(error => console.error('Error deleting list:', error));
}