const apiKey = '302d0bd906ba164038364ba764f6f413';
const apiToken = 'ATTA71b895ec11b616f6ad63f8a7ca27a65db6ee722415ffaf86bd7665401ad3e9b47ADD2EE5';

document.addEventListener("DOMContentLoaded", function () {
    fetchBoards();

    document.getElementById("show-create-board-form").addEventListener("click", function () {
        document.getElementById("create-board-form-container").classList.toggle("hidden");
    });

    document.getElementById("create-board-form").addEventListener("submit", function (event) {
        event.preventDefault();
        const boardName = document.getElementById("board-name").value;
        createBoard(boardName);
    });
});

function fetchBoards() {
    const url = `https://api.trello.com/1/members/me/boards?fields=name,url&key=${apiKey}&token=${apiToken}`;

    fetch(url)
        .then(response => response.json())
        .then(boards => {
            displayBoards(boards);
            populateSidebar(boards);
        })
        .catch(error => console.error('Error fetching boards:', error));
}

function displayBoards(boards) {
    const boardsContainer = document.getElementById("boards-container");
    boardsContainer.innerHTML = '';

    boards.forEach(board => {
        const boardElement = createBoardElement(board);
        boardsContainer.appendChild(boardElement);
    });

    const createButtonContainer = document.createElement('div');
    createButtonContainer.className = 'bg-gray-700 w-60 h-40 rounded shadow-md flex items-center justify-center';
    createButtonContainer.innerHTML = '<button type="button" id="show-create-board-form" class="font-bold text-gray-400">Create new board</button>';

    createButtonContainer.querySelector('button').addEventListener('click', () => {
        document.getElementById("create-board-form-container").classList.toggle("hidden");
    });
    boardsContainer.appendChild(createButtonContainer);
}

function createBoard(boardName) {
    const url = `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`;

    fetch(url, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Failed to create board');
            } else {
                return response.json();
            }
        })
        .then(board => {
            console.log('Board created:', board);
            addBoardToDOM(board);
            document.getElementById("create-board-form").reset();
            document.getElementById("create-board-form-container").classList.add("hidden");
        })
        .catch(error => console.error('Error:', error));
}

function addBoardToDOM(board) {
    const boardElement = createBoardElement(board);
    const boardsContainer = document.getElementById("boards-container");
    boardsContainer.insertBefore(boardElement, boardsContainer.lastElementChild);
}

function createBoardElement(board) {
    const boardContainer = document.createElement('div');
    boardContainer.className = 'bg-cover bg-center w-60 h-40 rounded shadow-md flex items-center justify-between px-2';
    boardContainer.dataset.id = board.id;

    const boardLink = document.createElement('a');
    boardLink.href = `boards_page.html?boardId=${board.id}`;
    boardLink.className = 'bg-blue-600 bg-opacity-50 h-full flex items-center justify-center rounded text-center flex-1';
    boardLink.innerHTML = `<span class="font-bold text-white">${board.name}</span>`;

    const deleteButton = document.createElement('img');
    deleteButton.addEventListener('click', (event) => {
        event.stopPropagation();
        event.preventDefault();
        deleteBoard(board.id);
    });

    boardContainer.appendChild(boardLink);
    boardContainer.appendChild(deleteButton);

    return boardContainer;
}

function populateSidebar(boards) {
    const boardsList = document.getElementById("boards-list");
    boardsList.innerHTML = '';

    boards.forEach(board => {
        const boardListItem = document.createElement('li');
        boardListItem.className = 'hover:text-gray-400 cursor-pointer flex justify-between items-center p-2';
        boardListItem.dataset.id = board.id;
        boardListItem.innerHTML = `<a href="boards_page.html?boardId=${board.id}" class="flex-1">${board.name}</a>`;

        const deleteButton = document.createElement('img');
        deleteButton.src = './images/trash_icon.png';
        deleteButton.alt = 'Delete';
        deleteButton.className = 'cursor-pointer w-4 h-4';

        deleteButton.addEventListener('click', (event) => {
            event.stopPropagation();
            event.preventDefault();
            deleteBoard(board.id);
        });

        boardListItem.appendChild(deleteButton);
        boardsList.appendChild(boardListItem);
    });
}

function deleteBoard(boardId) {
    const url = `https://api.trello.com/1/boards/${boardId}?key=${apiKey}&token=${apiToken}`;

    fetch(url, {
        method: 'DELETE'
    })
        .then(response => {
            if (response.ok) {
                document.querySelector(`div[data-id="${boardId}"]`).remove();
                document.querySelector(`li[data-id="${boardId}"]`).remove();
            } else {
                console.error('Error deleting board:', response.statusText);
            }
        })
        .catch(error => console.error('Error:', error));
}